﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class Department
    {
        [DataMember]
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        private bool isSelected;
        public bool IsSelected { get => isSelected; set => isSelected = value; }

        
    }
}