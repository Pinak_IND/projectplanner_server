﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class Wrapper
    {
        [DataMember]
        private MasterDataDetails masterDataWrapper;

        public MasterDataDetails MasterDataWrapper { get => masterDataWrapper; set => masterDataWrapper = value; }

        [DataMember]
        private List<TaskDetail> taskList;
        public List<TaskDetail> TaskList { get => taskList; set => taskList = value; }
    }
}