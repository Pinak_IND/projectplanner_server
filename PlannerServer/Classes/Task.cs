﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class Task
    {
        [DataMember]
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        private string taskName;
        public string TaskName
        {
            get { return taskName; }
            set { taskName = value; }
        }

        [DataMember]
        private string taskDescription;
        public string TaskDescription
        {
            get { return taskDescription; }
            set { taskDescription = value; }
        }

        [DataMember]
        private int priorityId;
        public int PriorityId
        {
            get { return priorityId; }
            set { priorityId = value; }
        }

        [DataMember]
        private string priorityName;
        public string PriorityName
        {
            get { return priorityName; }
            set { priorityName = value; }
        }

        [DataMember]
        private int taskTypeId;
        public int TaskTypeId
        {
            get { return taskTypeId; }
            set { taskTypeId = value; }
        }

        [DataMember]
        private string taskTypeName;
        public string TaskTypeName
        {
            get { return taskTypeName; }
            set { taskTypeName = value; }
        }

        [DataMember]
        private int projectId;
        public int ProjectId
        {
            get { return projectId; }
            set { projectId = value; }
        }

        [DataMember]
        private string projectName;
        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }

        [DataMember]
        private string technology;
        public string Technology
        {
            get { return technology; }
            set { technology = value; }
        }

        [DataMember]
        private int statusId;
        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        [DataMember]
        private string statusName;
        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        }

        [DataMember]
        private DateTime plannedstartDate;
        public DateTime PlannedStartDate
        {
            get { return plannedstartDate; }
            set { plannedstartDate = value; }
        }

        [DataMember]
        private DateTime plannedEndDate;
        public DateTime PlannedEndDate
        {
            get { return plannedEndDate; }
            set { plannedEndDate = value; }
        }
    }
}