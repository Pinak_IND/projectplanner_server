﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class MasterDataDetails
    {
        [DataMember]
        private List<Project> projectList;
        public List<Project> ProjectList
        {
            get { return projectList; }
            set { projectList = value; }
        }

        [DataMember]
        private List<Modules> moduleList;
        public List<Modules> ModuleList
        {
            get { return moduleList; }
            set { moduleList = value; }
        }

        [DataMember]
        private List<TaskType> taskTypeList;
        public List<TaskType> TaskTypeList
        {
            get { return taskTypeList; }
            set { taskTypeList = value; }
        }

        [DataMember]
        private List<Priority> priorityList;
        public List<Priority> PriorityList
        {
            get { return priorityList; }
            set { priorityList = value; }
        }

        [DataMember]
        private List<Department> departmentList;
        public List<Department> DepartmentList
        {
            get { return departmentList; }
            set { departmentList = value; }
        }
        [DataMember]
        private List<UserDetails> usersList;
        public List<UserDetails> UsersList
        {
            get { return usersList; }
            set { usersList = value; }
        }


        [DataMember]
        private List<StatusDetails> statusList;
        public List<StatusDetails> StatusList { get => statusList; set => statusList = value; }
    }
}