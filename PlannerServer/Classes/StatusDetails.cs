﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class StatusDetails
    {
        [DataMember]
        private int id;
        [DataMember]
        private string name;
        [DataMember]
        private string color;
        [DataMember]
        private int sequence;
        [DataMember]
        private bool isSelected;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Color { get => color; set => color = value; }
        public int Sequence { get => sequence; set => sequence = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
    }
}