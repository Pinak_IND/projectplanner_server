﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    
    [DataContract]
    public class TaskDetail
    {
        [DataMember]
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        private int projectId;
        public int ProjectId
        {
            get { return projectId; }
            set { projectId = value; }
        }

        [DataMember]
        private string projectName;
        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }

        [DataMember]
        private int moduleId;
        public int ModuleId
        {
            get { return moduleId; }
            set { moduleId = value; }
        }
        [DataMember]
        private string moduleName;
        public string ModuleName
        {
            get { return moduleName; }
            set { moduleName = value; }
        }
        [DataMember]
        private int deptId;
        public int DeptId
        {
            get { return deptId; }
            set { deptId = value; }
        }

        [DataMember]
        private string subject;
        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        [DataMember]
        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [DataMember]
        private int priorityId;
        public int PriorityId
        {
            get { return priorityId; }
            set { priorityId = value; }
        }

        [DataMember]
        private string priorityName;
        public string PriorityName
        {
            get { return priorityName; }
            set { priorityName = value; }
        }

        [DataMember]
        private int assigneeId;
        public int AssigneeId
        {
            get { return assigneeId; }
            set { assigneeId = value; }
        }

        [DataMember]
        private string assigneeName;
        public string AssigneeName
        {
            get { return assigneeName; }
            set { assigneeName = value; }
        }

        [DataMember]
        private int statusId;
        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        [DataMember]
        private string statusName;
        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        }

        [DataMember]
        private int taskTypeId;
        public int TaskTypeId
        {
            get { return taskTypeId; }
            set { taskTypeId = value; }
        }

        [DataMember]
        private string taskTypeName;
        public string TaskTypeName
        {
            get { return taskTypeName; }
            set { taskTypeName = value; }
        }

        [DataMember]
        private int duration;
        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        [DataMember]
        private DateTime? assignDate;
        public DateTime? AssignDate
        {
            get { return assignDate; }
            set { assignDate = value; }
        }

        [DataMember]
        private int? taskApproverId;
        public int? TaskApproverId
        {
            get { return taskApproverId; }
            set { taskApproverId = value; }
        }

        [DataMember]
        private string taskApproverName;
        public string TaskApproverName
        {
            get { return taskApproverName; }
            set { taskApproverName = value; }
        }

        [DataMember]
        private List<TaskActivity> activityList;
        public List<TaskActivity> ActivityList { get => activityList; set => activityList = value; }

        [DataMember]
        private string jiraId;
        public string JiraId { get => jiraId; set => jiraId = value; }

        [DataMember]
        private string creatorName;
        public string CreatorName
        {
            get { return creatorName; }
            set { creatorName = value; }
        }

        [DataMember]
        private int creatorId;
        public int CreatorId
        {
            get { return creatorId; }
            set { creatorId = value; }
        }

        
        [DataMember]
        private DateTime? createDate;
        public DateTime? CreateDate
        {
            get => createDate;
            set => createDate = value;
        }
        

        [DataMember]
        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get => lastModifiedDate;
            set => lastModifiedDate = value;
        }

    }
}