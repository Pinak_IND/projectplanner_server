﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class Project
    {
        [DataMember]
        private bool isSelected;
        [DataMember]
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        private string projectName;
        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }

        [DataMember]
        private String projectCode;
        public String ProjectCode
        {
            get { return projectCode; }
            set { projectCode = value; }
        }

        [DataMember]
        private string projectDescription;
        public string ProjectDescription
        {
            get { return projectDescription; }
            set { projectDescription = value; }
        }

        [DataMember]
        private int planTypeId;
        public int PlanTypeId
        {
            get { return planTypeId; }
            set { planTypeId = value; }
        }



        [DataMember]
        private string color;
        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        [DataMember]
        private DateTime startDateTime_Planned;
        public DateTime StartDateTime_Planned
        {
            get { return startDateTime_Planned; }
            set { startDateTime_Planned = value; }
        }

        [DataMember]
        private DateTime endDateTime_Planned;
        public DateTime EndDateTime_Planned
        {
            get { return endDateTime_Planned; }
            set { endDateTime_Planned = value; }
        }

        [DataMember]
        private int projectStatusId;
        public int ProjectStatusId
        {
            get { return projectStatusId; }
            set { projectStatusId = value; }
        }

        public bool IsSelected { get => isSelected; set => isSelected = value; }
    }
}