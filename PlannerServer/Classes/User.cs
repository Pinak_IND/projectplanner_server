﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class UserDetails
    {
        [DataMember]
        private string userName;
        [DataMember]
        private int userId;
        public int UserId { get => userId; set => userId = value; }
        [DataMember]
        private int roleId;
        [DataMember]
        private int deptId;
        [DataMember]
        private bool isAdmin;
        [DataMember]
        private string emailId;
        [DataMember]
        private bool isSelected;
        public string EmailId { get => emailId; set => emailId = value; }
        public int RoleId { get => roleId; set => roleId = value; }
        public int DeptId { get => deptId; set => deptId = value; }
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
        public string UserName { get => userName; set => userName = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
    }
}