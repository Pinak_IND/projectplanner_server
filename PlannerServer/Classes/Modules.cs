﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class Modules
    {
        [DataMember]
        private int id;
        [DataMember]
        private int projectId;
        [DataMember]
        private string name;
        [DataMember]
        private string desc;
        [DataMember]
        private DateTime? expectedStartDate;
        [DataMember]
        private DateTime? expectedEndDate;
        [DataMember]
        private int moduleLead;
        [DataMember]
        private DateTime? createdOn;
        [DataMember]
        private bool isSelected;

        public int Id { get => id; set => id = value; }
        public int ProjectId { get => projectId; set => projectId = value; }
        public string Name { get => name; set => name = value; }
        public string Desc { get => desc; set => desc = value; }
        public DateTime? ExpectedStartDate { get => expectedStartDate; set => expectedStartDate = value; }
        public DateTime? ExpectedEndDate { get => expectedEndDate; set => expectedEndDate = value; }
        public int ModuleLead { get => moduleLead; set => moduleLead = value; }
        public DateTime? CreatedOn { get => createdOn; set => createdOn = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
    }

}