﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    [DataContract]
    public class TaskActivity
    {
        [DataMember]
        private Guid id;
        [DataMember]
        private int taskId;
        [DataMember]
        private int statusId;
        [DataMember]
        private int userId;
        [DataMember]
        private string logMsg;
        [DataMember]
        private string desc;
        [DataMember]
        private DateTime? logDate;
        [DataMember]
        private string keyWord;
        [DataMember]
        private string userName;
        [DataMember]
        private int keyWordId;
        [DataMember]
        private int logUserId;
        [DataMember]
        private int priorityId;

        public Guid Id { get => id; set => id = value; }
        public int TaskId { get => taskId; set => taskId = value; }
        public int StatusId { get => statusId; set => statusId = value; }
        public int UserId { get => userId; set => userId = value; }
        public string LogMsg { get => logMsg; set => logMsg = value; }
        public string Desc { get => desc; set => desc = value; }
        public DateTime? LogDate { get => logDate; set => logDate = value; }
        public string KeyWord { get => keyWord; set => keyWord = value; }
        public string UserName { get => userName; set => userName = value; }
        public int KeyWordId { get => keyWordId; set => keyWordId = value; }
        public int LogUserId { get => logUserId; set => logUserId = value; }
        public int PriorityId { get => priorityId; set => priorityId = value; }
    }
}