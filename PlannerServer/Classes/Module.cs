﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PlannerServer
{
    
    [DataContract]
    public class EachUserTask
    {
        [DataMember]
        private int userid;
        public int Userid
        {
            get { return userid; }
            set { userid = value; }
        }


        [DataMember]
        private int taskid;
        public int Taskid
        {
            get { return taskid; }
            set { taskid = value; }
        }

        [DataMember]
        private string taskname;
        public string Taskname
        {
            get { return taskname; }
            set { taskname = value; }
        }


        [DataMember]
        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [DataMember]
        private int priorityid;
        public int Priorityid
        {
            get { return priorityid; }
            set { priorityid = value; }
        }

        [DataMember]
        private string icon;
        public string Icon
        {
            get { return icon; }
            set { icon = value; }
        }
        [DataMember]
        private int tasktypeid;
        public int Tasktypeid
        {
            get { return tasktypeid; }
            set { tasktypeid = value; }
        }

        [DataMember]
        private string tasktypename;
        public string Tasktypename
        {
            get { return tasktypename; }
            set { tasktypename = value; }
        }

        [DataMember]
        private int projectid;
        public int Projectid
        {
            get { return projectid; }
            set { projectid = value; }
        }

        [DataMember]
        private int moduleId;
        public int ModuleId
        {
            get { return moduleId; }
            set { moduleId = value; }
        }
        [DataMember]
        private int planTypeId;
        public int PlanTypeId { get => planTypeId; set => planTypeId = value; }

        [DataMember]
        private int projectstatusid;
        public int Projectstatusid
        {
            get { return projectstatusid; }
            set { projectstatusid = value; }
        }

        [DataMember]
        private int taskTypeId;
        public int TaskTypeId
        {
            get { return taskTypeId; }
            set { taskTypeId = value; }
        }

        [DataMember]
        private string project_name;
        public string Project_name
        {
            get { return project_name; }
            set { project_name = value; }
        }

        [DataMember]
        private string projectdescription;
        public string Projectdescription
        {
            get { return projectdescription; }
            set { projectdescription = value; }
        }

        [DataMember]
        private int statusid;
        public int Statusid
        {
            get { return statusid; }
            set { statusid = value; }
        }

        [DataMember]
        private string statusname;
        public string Statusname
        {
            get { return statusname; }
            set { statusname = value; }
        }



        [DataMember]
        private string statuscolor;
        public string Statuscolor
        {
            get { return statuscolor; }
            set { statuscolor = value; }
        }

        [DataMember]
        private int technologyId;
        public int TechnologyId
        {
            get { return technologyId; }
            set { technologyId = value; }
        }
        private string subject;
        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }


    }
}