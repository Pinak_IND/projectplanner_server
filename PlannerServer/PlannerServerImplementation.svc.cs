﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script;
using System.Web.Script.Serialization;
using static PlannerServer.IPlannerServerImplementation;

namespace PlannerServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TestServiceImplementation" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TestServiceImplementation.svc or TestServiceImplementation.svc.cs at the Solution Explorer and start debugging.
    public class PlannerServerImplementation : IPlannerServerImplementation
    {
        string connString = System.Configuration.ConfigurationManager.ConnectionStrings["connString"].ToString();
        public string JSONData(string time)
        {
            DateTime dt = Convert.ToDateTime(time.ToLower().Split('p')[0].Split('a')[0]);
            string _word = "Hello World @ " + Convert.ToString(dt);
            return _word;
        }
        
        public TaskDetail ChangingTaskProperties(int selectedRowId, string usedFor, int changedValue, int logUserId)
        {
            TaskDetail ToReturn = new TaskDetail();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.CommandText = "usp_ChangingTaskProperties";
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@TaskId", SqlDbType.Int).Value = selectedRowId;
                    cmd.Parameters.Add("@UsedFor", SqlDbType.VarChar).Value = usedFor;
                    cmd.Parameters.Add("@ChangedValue", SqlDbType.Int).Value = changedValue;
                    cmd.Parameters.Add("@LogMsg", SqlDbType.VarChar).Value = usedFor == "Priority" ? "changes priority" : usedFor == "Assignee" ? "assign to" : string.Empty ;
                    cmd.Parameters.Add("@LogDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("@LogUserId", SqlDbType.Int).Value = logUserId;
                    
                    conn.Open();

                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(cmd);

                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    DataTable SelectedTask = ds.Tables[0];
                    DataTable TaskActivityTable = ds.Tables[1];

                    foreach (DataRow row in SelectedTask.Rows)
                    {
                        TaskDetail eachTask = new TaskDetail();

                        eachTask.Id = Convert.ToInt32(row["Id"]);
                        eachTask.ProjectId = Convert.ToInt32(row["ProjectId"]);
                        eachTask.ProjectName = row["ProjectName"].ToString();
                        eachTask.ModuleId = !DBNull.Value.Equals(row["ModuleId"]) ? Convert.ToInt32(row["ModuleId"]) : 0;
                        eachTask.ModuleName = !DBNull.Value.Equals(row["ModuleName"]) ? row["ModuleName"].ToString() : string.Empty;
                        eachTask.Subject = !DBNull.Value.Equals(row["Subject"]) ? row["Subject"].ToString() : string.Empty;
                        eachTask.Description = row["Description"].ToString();
                        eachTask.PriorityId = Convert.ToInt32(row["PriorityId"]);
                        eachTask.PriorityName = row["TaskPriority"].ToString();
                        eachTask.AssigneeId = !DBNull.Value.Equals(row["AssigneeId"]) ? Convert.ToInt32(row["AssigneeId"]) : 0;
                        eachTask.AssigneeName = !DBNull.Value.Equals(row["AssigneeId"]) ? row["AssigneeName"].ToString() : string.Empty;
                        eachTask.StatusId = Convert.ToInt32(row["StatusId"]);
                        eachTask.StatusName = row["StatusName"].ToString();
                        eachTask.TaskTypeId = Convert.ToInt32(row["TaskTypeId"]);
                        eachTask.TaskTypeName = row["TaskTypeName"].ToString();
                        eachTask.Duration = !DBNull.Value.Equals(row["Duration"]) ? Convert.ToInt32(row["Duration"]) : 0;
                        eachTask.AssignDate = !DBNull.Value.Equals(row["AssignDate"]) ? Convert.ToDateTime(row["AssignDate"]) : new DateTime?();
                        eachTask.TaskApproverId = !DBNull.Value.Equals(row["TaskApproverId"]) ? Convert.ToInt32(row["TaskApproverId"]) : 0;
                        eachTask.TaskApproverName = !DBNull.Value.Equals(row["TaskApproverName"]) ? row["TaskApproverName"].ToString() : string.Empty;
                        eachTask.JiraId = !DBNull.Value.Equals(row["JiraId"]) ? Convert.ToString(row["JiraId"]) : string.Empty;
                        eachTask.CreatorId = !DBNull.Value.Equals(row["CreatorId"]) ? Convert.ToInt32(row["CreatorId"]) : 0;
                        eachTask.CreateDate = !DBNull.Value.Equals(row["CreateDate"]) ? Convert.ToDateTime(row["CreateDate"]) : new DateTime();
                        eachTask.LastModifiedDate = !DBNull.Value.Equals(row["LastModifiedDate"]) ? Convert.ToDateTime(row["LastModifiedDate"]) : new DateTime();

                        eachTask.ActivityList = new List<TaskActivity>();

                        foreach (DataRow eachActivity in TaskActivityTable.Rows)
                        {
                            if (Convert.ToInt32(eachActivity["TaskId"]) == eachTask.Id)
                            {
                                TaskActivity Ta = new TaskActivity();
                                Guid g = new Guid(eachActivity["ID"].ToString());
                                Ta.Id = g;
                                Ta.TaskId = eachTask.Id;
                                Ta.StatusId = eachTask.StatusId;
                                Ta.UserId = !DBNull.Value.Equals(eachActivity["UserId"]) ? Convert.ToInt32(eachActivity["UserId"]) : 0;
                                Ta.LogMsg = !DBNull.Value.Equals(eachActivity["LogMsg"]) ? eachActivity["LogMsg"].ToString() : string.Empty;
                                Ta.KeyWord = eachActivity["KeyWord"].ToString();
                                Ta.Desc = eachActivity["Description"].ToString();
                                Ta.LogDate = !DBNull.Value.Equals(eachActivity["LogDate"]) ? Convert.ToDateTime(eachActivity["LogDate"]) : new DateTime?();
                                Ta.UserName = Convert.ToString(eachActivity["UserName"]);
                                Ta.KeyWordId = Convert.ToInt32(eachActivity["KeyWordId"]);
                                Ta.LogUserId = !DBNull.Value.Equals(eachActivity["LogUserId"]) ? Convert.ToInt32(eachActivity["LogUserId"]) : 0;
                                Ta.PriorityId = !DBNull.Value.Equals(eachActivity["PriorityId"]) ? Convert.ToInt32(eachActivity["PriorityId"]) : 0;

                                eachTask.ActivityList.Add(Ta);
                            }
                        }
                        eachTask.ActivityList = eachTask.ActivityList.OrderBy(a => a.LogDate).ToList();
                        ToReturn = eachTask;
                    }
                }
            }


            return ToReturn;
        }

        public Wrapper GetDefaultData(int userId)
        {
            Wrapper ToReturn = new Wrapper();

            ToReturn.MasterDataWrapper = FetchMasterData();
            ToReturn.TaskList = FetchTaskList(userId);

            return ToReturn;
        }

        public List<TaskDetail> FetchTaskList(int userId)
        {
            List<TaskDetail> ToReturn = new List<TaskDetail>();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.CommandText = "usp_ShowAllTask";
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();

                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(cmd);

                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    DataTable MasterTaskTable = ds.Tables[0];
                    DataTable TaskActivityTable = ds.Tables[1];

                    foreach (DataRow row in MasterTaskTable.Rows)
                    {
                        TaskDetail eachTask = new TaskDetail();

                        eachTask.Id = Convert.ToInt32(row["Id"]);
                        eachTask.ProjectId = Convert.ToInt32(row["ProjectId"]);
                        eachTask.ProjectName = row["ProjectName"].ToString();
                        eachTask.ModuleId = !DBNull.Value.Equals(row["ModuleId"]) ? Convert.ToInt32(row["ModuleId"]) : 0;
                        eachTask.ModuleName = !DBNull.Value.Equals(row["ModuleName"]) ? row["ModuleName"].ToString() : string.Empty;
                        eachTask.Subject = !DBNull.Value.Equals(row["Subject"]) ? row["Subject"].ToString() : string.Empty;
                        eachTask.Description = row["Description"].ToString();
                        eachTask.PriorityId = Convert.ToInt32(row["PriorityId"]);
                        eachTask.PriorityName = row["TaskPriority"].ToString();
                        eachTask.AssigneeId = !DBNull.Value.Equals(row["AssigneeId"]) ? Convert.ToInt32(row["AssigneeId"]) : 0;
                        eachTask.AssigneeName = !DBNull.Value.Equals(row["AssigneeId"]) ? row["AssigneeName"].ToString() : string.Empty;
                        eachTask.StatusId = Convert.ToInt32(row["StatusId"]);
                        eachTask.StatusName = row["StatusName"].ToString();
                        eachTask.TaskTypeId = Convert.ToInt32(row["TaskTypeId"]);
                        eachTask.TaskTypeName = row["TaskTypeName"].ToString();
                        eachTask.Duration = !DBNull.Value.Equals(row["Duration"]) ? Convert.ToInt32(row["Duration"]) : 0;
                        eachTask.AssignDate = !DBNull.Value.Equals(row["AssignDate"]) ? Convert.ToDateTime(row["AssignDate"]) : new DateTime?();
                        eachTask.TaskApproverId = !DBNull.Value.Equals(row["TaskApproverId"]) ? Convert.ToInt32(row["TaskApproverId"]) : 0;
                        eachTask.TaskApproverName = !DBNull.Value.Equals(row["TaskApproverName"]) ? row["TaskApproverName"].ToString() : string.Empty;
                        eachTask.JiraId = !DBNull.Value.Equals(row["JiraId"]) ? Convert.ToString(row["JiraId"]) : string.Empty;
                        eachTask.CreatorId = !DBNull.Value.Equals(row["CreatorId"]) ? Convert.ToInt32(row["CreatorId"]) : 0;
                        eachTask.CreateDate = !DBNull.Value.Equals(row["CreateDate"]) ? Convert.ToDateTime(row["CreateDate"]) : new DateTime();
                        eachTask.LastModifiedDate = !DBNull.Value.Equals(row["LastModifiedDate"]) ? Convert.ToDateTime(row["LastModifiedDate"]) : new DateTime();
                        eachTask.CreatorName = !DBNull.Value.Equals(row["CreatorName"]) ? Convert.ToString(row["CreatorName"]) : string.Empty;

                        eachTask.ActivityList = new List<TaskActivity>();

                        foreach (DataRow eachActivity in TaskActivityTable.Rows)
                        {
                            if(Convert.ToInt32(eachActivity["TaskId"]) == eachTask.Id)
                            {
                                TaskActivity Ta = new TaskActivity();
                                Guid g = new Guid(eachActivity["ID"].ToString());
                                Ta.Id = g;
                                Ta.TaskId = eachTask.Id;
                                Ta.StatusId = eachTask.StatusId;
                                Ta.UserId = !DBNull.Value.Equals(eachActivity["UserId"]) ? Convert.ToInt32(eachActivity["UserId"]) : 0;
                                Ta.LogMsg = eachActivity["LogMsg"].ToString();
                                Ta.KeyWord = eachActivity["KeyWord"].ToString();
                                Ta.Desc = eachActivity["Description"].ToString();
                                Ta.LogDate = !DBNull.Value.Equals(eachActivity["LogDate"]) ? Convert.ToDateTime(eachActivity["LogDate"]) : new DateTime? ();
                                Ta.UserName = Convert.ToString(eachActivity["UserName"]);
                                Ta.KeyWordId = Convert.ToInt32(eachActivity["KeyWordId"]);
                                Ta.LogUserId = !DBNull.Value.Equals(eachActivity["LogUserId"]) ? Convert.ToInt32(eachActivity["LogUserId"]) : 0;
                                Ta.PriorityId = !DBNull.Value.Equals(eachActivity["PriorityId"]) ? Convert.ToInt32(eachActivity["PriorityId"]) : 0;

                                eachTask.ActivityList.Add(Ta);
                            }
                        }

                        ToReturn.Add(eachTask);

                    }

                    conn.Close();
                }
            }

            //string connString = System.Configuration.ConfigurationManager.ConnectionStrings["connString"].ToString();
            //using (SqlConnection con = new SqlConnection(connString))
            //{
            //    SqlCommand cmd = new SqlCommand("usp_ShowAllTask", con);
            //    cmd.CommandType = CommandType.StoredProcedure;// doing same as GetAllTaskFromDB  
            //    con.Open();
            //    using (SqlDataReader reader = cmd.ExecuteReader())
            //    {
            //        while (reader.Read())
            //        {
            //            TaskDetail eachTask = new TaskDetail();
            //            eachTask.Id = Convert.ToInt32(reader["Id"]);
            //            eachTask.ProjectId = Convert.ToInt32(reader["ProjectId"]);
            //            eachTask.ProjectName = reader["ProjectName"].ToString();
            //            eachTask.ModuleId = !DBNull.Value.Equals(reader["ModuleId"]) ? Convert.ToInt32(reader["ModuleId"]) : 0;
            //            eachTask.ModuleName = !DBNull.Value.Equals(reader["ModuleName"]) ? reader["ModuleName"].ToString() : string.Empty;
            //            eachTask.Subject = !DBNull.Value.Equals(reader["Subject"]) ? reader["Subject"].ToString() : string.Empty;
            //            eachTask.Description = reader["Description"].ToString();
            //            eachTask.PriorityId = Convert.ToInt32(reader["PriorityId"]);
            //            eachTask.PriorityName = reader["TaskPriority"].ToString();
            //            eachTask.AssigneeId = !DBNull.Value.Equals(reader["AssigneeId"]) ? Convert.ToInt32(reader["AssigneeId"]) : 0;
            //            eachTask.AssigneeName = !DBNull.Value.Equals(reader["AssigneeId"]) ? reader["AssigneeName"].ToString() : string.Empty;
            //            eachTask.StatusId = Convert.ToInt32(reader["StatusId"]);
            //            eachTask.StatusName = reader["StatusName"].ToString();
            //            eachTask.TaskTypeId = Convert.ToInt32(reader["TaskTypeId"]);
            //            eachTask.TaskTypeName = reader["TaskTypeName"].ToString();
            //            eachTask.Duration = !DBNull.Value.Equals(reader["Duration"]) ? Convert.ToInt32(reader["Duration"]) : 0;
            //            eachTask.AssignDate = !DBNull.Value.Equals(reader["AssignDate"]) ? Convert.ToDateTime(reader["AssignDate"]) : new DateTime?();
            //            eachTask.TaskApproverId = !DBNull.Value.Equals(reader["TaskApproverId"]) ? Convert.ToInt32(reader["TaskApproverId"]) : 0;
            //            eachTask.TaskApproverName = !DBNull.Value.Equals(reader["TaskApproverName"]) ? reader["TaskApproverName"].ToString() : string.Empty;
            //            eachTask.JiraId = !DBNull.Value.Equals(reader["JiraId"]) ? Convert.ToString(reader["JiraId"]) : string.Empty;
            //            eachTask.CreatorId = !DBNull.Value.Equals(reader["CreatorId"]) ? Convert.ToInt32(reader["CreatorId"]) : 0;
                        
            //            ToReturn.Add(eachTask);
            //        }
            //    }
            //    con.Close();
            //}
            return ToReturn;
        }

        public MasterDataDetails FetchMasterData()
        {
            var MasterData = new MasterDataDetails();
            //Parallel.Invoke(
                //() =>
                //{
                //    MasterData.ProjectList = FetchMasterProject();
                //},
                //() =>
                //{
                //    MasterData.ModuleList = FetchMasterModule();
                //},
                //() =>
                //{
                //    MasterData.DepartmentList = FetchDepartment();
                //},
                //() =>
                //{
                //    MasterData.PriorityList = FetchPriority();
                //},
                //() =>
                //{
                //    MasterData.TaskTypeList = FetchTaskType();
                //},
                //() =>
                //{
                //    MasterData.UsersList = FetchUserList();
                //},
                //() =>
                //{
                //    MasterData.StatusList = FetchStatusList();
                //});
            MasterData.ProjectList = FetchMasterProject();
            MasterData.ModuleList = FetchMasterModule();
            MasterData.DepartmentList = FetchDepartment();
            MasterData.PriorityList = FetchPriority();
            MasterData.TaskTypeList = FetchTaskType();
            MasterData.UsersList = FetchUserList();
            MasterData.StatusList = FetchStatusList();
            return MasterData;
        }

        public List<Project> FetchMasterProject()
        {
            List<Project> ProjectList = new List<Project>();
            Project Project = new Project();

            string query = "Select * from Master_Project";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProjectList.Add(new Project()
                            {
                                Id = reader.GetInt32(0),
                                ProjectName = reader.GetString(1),
                                ProjectCode = reader.GetString(2),
                                ProjectDescription = reader.GetString(3),
                                PlanTypeId = reader.GetInt32(4),
                                Color = reader.GetString(5),
                                StartDateTime_Planned = reader.GetDateTime(6),
                                EndDateTime_Planned = reader.GetDateTime(7),
                                ProjectStatusId = reader.GetInt32(8)
                            });
                        }
                    }
                }
                con.Close();
                ProjectList[0].IsSelected = true;
                return ProjectList;
            }
        }
        public List<Modules> FetchMasterModule()
        {
            List<Modules> ModuleList = new List<Modules>();
            string query = "Select * from Master_Module";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ModuleList.Add(new Modules()
                            {
                                Id = reader.GetInt32(0),
                                ProjectId = reader.GetInt32(1),
                                Name = reader.GetString(2),
                                Desc = reader.GetString(3),
                                ExpectedStartDate = reader.IsDBNull(4) ? System.DateTime.Now : reader.GetDateTime(4),
                                ExpectedEndDate = reader.IsDBNull(5) ? System.DateTime.Now : reader.GetDateTime(5),
                                ModuleLead = reader.GetInt32(6),
                                CreatedOn = reader.GetDateTime(7)

                            });
                        }
                    }
                }
                con.Close();
                ModuleList[0].IsSelected = true;
                return ModuleList;
            }

        }
        public List<TaskType> FetchTaskType()
        {
            List<TaskType> TaskTypeList = new List<TaskType>();
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand("ListTaskType", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TaskType Tasktype = new TaskType();
                        Tasktype.Id = Convert.ToInt16(reader["Id"]);
                        Tasktype.Name = reader["Name"].ToString();

                        TaskTypeList.Add(Tasktype);
                    }
                }
                TaskTypeList[0].IsSelected = true;
                con.Close();
            }
            return TaskTypeList;
        }
        public List<Department> FetchDepartment()
        {
            List<Department> DepartmentList = new List<Department>();
            string query = "Select * from Master_Department";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DepartmentList.Add(new Department()
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1)
                            });
                        }
                    }
                }
                con.Close();
                DepartmentList[0].IsSelected = true;
                return DepartmentList;
            }
        }
        public List<Priority> FetchPriority()
        {
            List<Priority> PriorityList = new List<Priority>();
            string query = "Select * from Master_Priority";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PriorityList.Add(new Priority()
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Icon = reader.GetString(2),
                                Sequence = reader.GetInt32(3),
                            });
                        }
                    }
                }
                con.Close();
                return PriorityList;
            }
        }

        public List<UserDetails> FetchUserList()
        {
            List<UserDetails> PriorityList = new List<UserDetails>();
            string query = "Select * from Master_User";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PriorityList.Add(new UserDetails()
                            {
                                UserId = reader.GetInt32(0),
                                UserName = reader.GetString(1),
                                EmailId = reader.GetString(2),
                                RoleId = reader.GetInt32(3),
                                DeptId = reader.GetInt32(4),
                                IsAdmin = reader.GetBoolean(5)

                            });
                        }
                    }
                }
                con.Close();
                return PriorityList;
            }
        }

        public List<StatusDetails> FetchStatusList()
        {
            List<StatusDetails> StatusList = new List<StatusDetails>();
            string query = "Select * from Master_Status";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StatusList.Add(new StatusDetails()
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Color = reader.GetString(2),
                                Sequence = reader.GetInt32(3),
                            });
                        }
                    }
                }
                con.Close();
                return StatusList;
            }
        }

        public List<UserDetails> FetchAdminList()
        {
            List<UserDetails> PriorityList = new List<UserDetails>();
            string query = "Select * from Master_User where [IsAdmin] = 1";
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandTimeout = 0;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PriorityList.Add(new UserDetails()
                            {
                                UserId = reader.GetInt32(0),
                                UserName = reader.GetString(1),
                                EmailId = reader.GetString(2),
                                RoleId = reader.GetInt32(3),
                                DeptId = reader.GetInt32(4),
                                IsAdmin = reader.GetBoolean(5)

                            });
                        }
                    }
                }
                con.Close();
                return PriorityList;
            }
        }

        public string SavingTask(string taskToSave)
        {
            var eachProperty = taskToSave.Split('*').ToList();
            var dict = new Dictionary<string, string>();
            foreach (var each in eachProperty)
            {
                if (each != "")
                {
                    dict.Add(each.Split(',').ToList()[0], each.Split(',').ToList()[1]);
                }
            }

            #region TimeConversion
            int times = 0;
            string time = dict.FirstOrDefault(kvp => kvp.Key.Contains("Duration")).Value;
            if(time.Length == 3) { times = Convert.ToInt32(time.Substring(0, 2)); }
            else { times = (Convert.ToInt32(time.Substring(0, 1)) * 60) + (Convert.ToInt32(time.Substring(3, 2))); }
            #endregion


            string outputString = string.Empty;
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("usp_createTask", con))
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ProjectId", SqlDbType.Int).Value = dict["ProjectId"];
                        cmd.Parameters.Add("@Subject", SqlDbType.VarChar, 8000).Value = dict["Subject"];
                        cmd.Parameters.Add("@Desc", SqlDbType.VarChar, 8000).Value = dict["Description"];
                        cmd.Parameters.Add("@PriorityId", SqlDbType.Int).Value = dict["priorityId"];
                        cmd.Parameters.Add("@TaskTypeId", SqlDbType.Int).Value = dict["TaskTypeId"];
                        cmd.Parameters.Add("@StatusId", SqlDbType.Int).Value = dict["statusId"];
                        cmd.Parameters.Add("@ModuleId", SqlDbType.Int).Value = dict["ModuleId"];
                        cmd.Parameters.Add("@TechnologyId", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@DepartmentId", SqlDbType.Int).Value = dict["DeptId"];
                        cmd.Parameters.Add("@AssigneeId", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@AssignDate",  SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@Duration", SqlDbType.Int).Value = times;
                        cmd.Parameters.Add("@TaskApproverId", SqlDbType.Int).Value = dict["TaskApproverId"];
                        cmd.Parameters.Add("@CreatorId", SqlDbType.Int).Value = dict["CreatorId"];
                        cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@LastModifiedDate", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@Jira", SqlDbType.VarChar, 8000).Value = dict["Jira"];
                        cmd.Parameters.Add("@LogDate", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("@LogMsg", SqlDbType.VarChar, 8000).Value = "raised a request";

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                outputString = reader.GetString(0);
                            }
                        }
                    }
                    con.Close();
                }
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
            return outputString;
        }
    }
}
