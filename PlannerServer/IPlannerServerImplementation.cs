﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PlannerServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITestServiceImplementation" in both code and config file together.
    [ServiceContract]
    public interface IPlannerServerImplementation
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "JSONData?time={time}"
            )]
        string JSONData(string time);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "FetchTaskList?userId={userId}"
            )]
        List<TaskDetail> FetchTaskList(int userId);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "FetchMasterData"
            )]
        MasterDataDetails FetchMasterData();

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "GetDefaultData?userId={userId}"
            )]
        Wrapper GetDefaultData(int userId);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "ChangingTaskProperties?selectedRowId={selectedRowId}&usedFor={usedFor}&changedValue={changedValue}&logUserId={logUserId}"
            )]
        TaskDetail ChangingTaskProperties(int selectedRowId, string usedFor, int changedValue, int logUserId);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.WrappedRequest, 
            UriTemplate = "SavingTask?taskToSave={taskToSave}")]//
        string SavingTask(string taskToSave);
    }

}
